/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipc_arrelgos;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 *
 * @author William Corado
 */
public class reporteHTML {

    public reporteHTML(int[]arreglo) throws FileNotFoundException {
        generarReporte(arreglo);
    }
    
    public void generarReporte(int[]arreglo) throws FileNotFoundException{
        // se crea el archivo o se modifica si existiera
        FileOutputStream file = new FileOutputStream("reporte.html");
        // Sirve como puntero para imprimir en el archivo
        PrintStream p = new PrintStream(file);
        
        // imprimen
        p.println("<html><body><table><tr><td>Valor</td></tr>");
        
        for (int i = 0; i < arreglo.length; i++) {
            p.println("<tr><td>"+arreglo[i]+"</td></tr>");
        }
        
        p.println("</table></body></html>");
        p.close();
        
    }
}
