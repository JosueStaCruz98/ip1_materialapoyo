import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';
import { Product } from '../../models/product'

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {

  product: Product = {
    id:0,
    nombre:'',
    precio:0,
    cantidad:0,
    tipo:''
  }; 

  constructor(private backend:BackendService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    console.log(this.product)
    this.backend.newProduct(this.product)
    .toPromise()
    .then(() => alert('Cargado correctamente'))
    .catch(() => alert('hubo un problema'))
  }

}
