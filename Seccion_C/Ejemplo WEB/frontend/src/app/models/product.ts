export interface Product {
    id?: number,
    nombre?: string,
    precio?: number,
    cantidad?: number,
    tipo?: string
};