import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) { }

  URL: string = 'http://localhost:5000';

  getProducts(){
    return this.http.get(`${this.URL}/producto`) 
  }

  newProduct(libro:any){
    return this.http.post(`${this.URL}/producto`,libro)
  }

}
