package ejemplospractica12s2021;
import java.util.Scanner;
import java.util.Random;

public class iniciojuego {
	public static int posicion =0;
	public static boolean estadomenu = false;
	public static Scanner miescaner = new Scanner(System.in);
	public static String[][] tablero = new String[8][8];
	public static String ESPACIO_VACIO ="     "; // 5 spaces
	public static String UBICACION ="   # "; // 5 spaces
	public static String SEPARADOR_FILAS = "==================================================";
	
	// DATOS OPERACIONES INTERMEDIAS
	public static int[][] matrizA_1	 = new int[5][5];
	public static int[][] matrizB_1 = new int[5][5];
	
	public static int[][] matrizA_2 = {{7,48,5,0,1},{57,8,4,6,14}};
	public static int[][] matrizB_2 = new int[5][5];
	
	public static int[][] matrizA_3 = new int[5][5];
	public static int[][] matrizB_3 = new int[5][5];
	
	public static boolean inter1=false,inter2=false,inter3=false;
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MenuPrincipal();
		
	}
	
	
	public static void MenuPrincipal() {
		
		while(estadomenu == false) {
			System.out.println("********** MENU PRINCIPAL ***************");
			System.out.println("1. Iniciar Juego");
			System.out.println("2. Generar Reportes");
			System.out.println("3. Salir");
			System.out.println("Eliga el numero de la opción");
			int opcion = miescaner.nextInt();
			
			if(opcion == 1) {
				Inicio_juego();
			}else if(opcion ==2) {
				// Generar reportes
			}else if(opcion == 3) {
				estadomenu = true;
			}
		
		}
		
	}
	
	
	public static void Inicio_juego() {
		Llenar_matriz();
		// Colocar penalizaciones
		Imprimir_Matriz();
		int opcion= miescaner.nextInt();
		
		
	}
	
	public static void Llenar_matriz() {
		int contador =0;
		for (int i = 0; i < tablero.length; i++) {
			// Movimiento por filas
			for (int j = 0; j < tablero[i].length; j++) {
				// movimiento columnas
				tablero[i][j]=ESPACIO_VACIO;
				contador++;
				
			}
		}
	}
	
	public static void Imprimir_Matriz() {
		for (int i = 0; i < tablero.length; i++) {
			// Movimiento por filas
			System.out.println(SEPARADOR_FILAS);
			
			for (int j = 0; j < tablero[i].length; j++) {
				// movimiento columnas
				System.out.print("|");
				System.out.print(tablero[i][j]);

			}
			System.out.print("|");
			System.out.println("");	
			
			for (int j = 0; j < tablero[i].length; j++) {
				// movimiento columnas
				System.out.print("|");
				System.out.print(tablero[i][j]);

			}
			
			System.out.print("|");
			System.out.println("");		
		}
		System.out.println(SEPARADOR_FILAS);
	}
	
	public static void Elegir_Intermedias() {
		if(inter1==false && inter2 ==false && inter3 ==false) {
			int min =1;
			int max =3;
			Random random = new Random();
			int valor = random.nextInt(max-min)+min;	
			Operar_Intermedias(valor);
		}else if(inter1==true && inter2 ==false && inter3 ==false) {
			int min =2;
			int max =3;
			Random random = new Random();
			int valor = random.nextInt(max-min)+min;	
			Operar_Intermedias(valor);
		}else if(inter1== false && inter2 ==true && inter3 ==false) {
			int valor = 0;
			int min =1;
			int max =3;
			Random random = new Random();
			while(valor!=2) {
			 valor = random.nextInt(max-min)+min;
			
			}
			Operar_Intermedias(valor);
		}

	}
	
	
	
	
	public static void Operar_Intermedias(int numero) {
		if(numero == 1) {
			Resolver_Suma(matrizA_1,matrizB_1);
			inter1=true;
		}else if(numero == 2) {
			Resolver_Suma(matrizA_2,matrizB_2);
			inter2 =true;
		}else if(numero == 3) {
			Resolver_Suma(matrizA_3,matrizB_3);
			inter3=true;
		}
	}
	
	public static void Resolver_Suma(int[][] a,int[][] b) {
		int [][] matriz_resultado = new int [5][5];
		int filasa= a.length;
		int filasb = b.length;
		int columnas_a = a[0].length;
		int columnas_b = b[0].length;
		
		if(filasa == filasb && columnas_a == columnas_b) {
			// matriz valida
			for (int i = 0; i < a.length; i++) {
				for (int j = 0; j < a[0].length; j++) {
					matriz_resultado[i][j]=a[i][j]+b[i][j];
					
				}
			}
		}
		

		
		
	}
	
	

}
