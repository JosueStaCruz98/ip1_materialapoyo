package ejemplopractica2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.AncestorListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Stack;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class pantallajuego extends JFrame implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	public JLabel [] discos = new JLabel[3]; // se van a trabajar unicamente con 3 discos, para su practica deben ser de 3 a 7
	Stack<JLabel> torre1 = new Stack<JLabel>(); // este tipo de dato nos permite  trabajar como pila
	Stack<JLabel> torre2 = new Stack<JLabel>();
	Stack<JLabel> torre3 = new Stack<JLabel>();
	Boolean puntarojo = false;
	boolean terminatiempo;
	
	JLabel labelhora;
	
	public  int tiempojuego =120; // yo voy a dejar 120 segundos por default, ustedes deben manejar un tiempo  segun el usuario eliga en la configuracion
	
	/* Declaramos los hilos que vamos a usar*/
	
	public Thread hilohora;
	public Thread hilopotenciador;
	public Thread hilopenalizacion;
	
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					pantallajuego frame = new pantallajuego();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public pantallajuego() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 756, 456);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton derechatorre1 = new JButton(">");
		derechatorre1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// de la torre 1 muevo a la torre 2
				if(torre1.size()>0) {
					JLabel amover = torre1.peek();
					comparar_torre2(amover,1);
				}

				
			}
		});
		derechatorre1.setBounds(196, 278, 72, 31);
		contentPane.add(derechatorre1);
		
		JButton izquierdatorre1 = new JButton("<");
		izquierdatorre1.setBounds(114, 278, 72, 31);
		contentPane.add(izquierdatorre1);
		
		JButton izquierdatorre2 = new JButton("<");
		izquierdatorre2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(torre2.size()>0) {
					JLabel amover = torre2.peek();
					comparar_torre1(amover,2);
				}

			}
		});
		izquierdatorre2.setBounds(310, 278, 72, 31);
		contentPane.add(izquierdatorre2);
		
		JButton derechatorre2 = new JButton(">");
		derechatorre2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(torre2.size()>0) {
					JLabel amover = torre2.peek();
					comparar_torre3(amover,2);
				}

			}
		});
		derechatorre2.setBounds(392, 278, 72, 31);
		contentPane.add(derechatorre2);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setOpaque(true);
		lblNewLabel.setBackground(Color.GRAY);
		lblNewLabel.setForeground(SystemColor.desktop);
		lblNewLabel.setBounds(127, 261, 130, 7);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setOpaque(true);
		lblNewLabel_2.setForeground(SystemColor.desktop);
		lblNewLabel_2.setBackground(Color.GRAY);
		lblNewLabel_2.setBounds(312, 261, 130, 7);
		contentPane.add(lblNewLabel_2);
		
		JButton izquierdatorre3 = new JButton("<");
		izquierdatorre3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(torre3.size()>0) {
					JLabel amover = torre3.peek();
					comparar_torre2(amover,3);
				}

			}
		});
		izquierdatorre3.setBounds(512, 278, 72, 31);
		contentPane.add(izquierdatorre3);
		
		JButton derechatorre3 = new JButton(">");
		derechatorre3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		derechatorre3.setBounds(606, 278, 72, 31);
		contentPane.add(derechatorre3);
		
		JLabel lblNewLabel_2_1 = new JLabel("");
		lblNewLabel_2_1.setOpaque(true);
		lblNewLabel_2_1.setForeground(SystemColor.desktop);
		lblNewLabel_2_1.setBackground(Color.GRAY);
		lblNewLabel_2_1.setBounds(524, 261, 130, 7);
		contentPane.add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("");
		lblNewLabel_1_1.setOpaque(true);
		lblNewLabel_1_1.setBackground(Color.GRAY);
		lblNewLabel_1_1.setBounds(373, 151, 9, 117);
		contentPane.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("");
		lblNewLabel_1_1_1.setOpaque(true);
		lblNewLabel_1_1_1.setBackground(Color.GRAY);
		lblNewLabel_1_1_1.setBounds(588, 151, 9, 117);
		contentPane.add(lblNewLabel_1_1_1);
		
	
	
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBackground(Color.GRAY);
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setBounds(186, 151, 9, 117);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_3 = new JLabel("Tiempo");
		lblNewLabel_3.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		lblNewLabel_3.setBounds(37, 37, 79, 31);
		contentPane.add(lblNewLabel_3);
		
		labelhora = new JLabel("-");
		labelhora.setFont(new Font("Tahoma", Font.PLAIN, 15));
		labelhora.setBounds(114, 36, 72, 24);
		contentPane.add(labelhora);
		
		
		Colocacion_inicial();
		
		// mando a llamar a los hilos
		hilohora = new Thread(this);
		hilopotenciador = new Thread(this);
		
		hilohora.start();
		hilopotenciador.start();

		
	}
	
	public void Colocacion_inicial() {
		// este metodo unicamnte se va a ejecutar cuando se inicia un nuevo juego
		// lo que  hacemos es meter los discos a la lista de la torre 1
		// como es el inicio, las demas torres van a estar vacias
		
		for(int i = 0;i<discos.length;i++) { // vamos a inicializar los 3  discos y colocarlos en sus posiciones incial
			discos[i] = new JLabel();
			// como cada disco tiene diferente tama�o y ubicacion, vamos a tener un case para crearlos personalizados
			// Mi logica sera que el disco 0 sera el mas peque�o, ustedes lo pueden trabajar diferente.
			switch(i){
			// uso el setbound por que ahi colo de una vez la ubicacion y tama�o
			case 0:
				discos[i].setBounds(174,216,40,15);
				discos[i].setBackground(Color.red);
				break;
			case 1:											// las posiciones las fui probando a mano, pero pueden colocarlas con drag a drop, ver donde quedan mejor y ya luego solo apuntar los datos y borrar los labels
				discos[i].setBounds(154,231,80,15);
				discos[i].setBackground(Color.blue);
				break;
			case 2:
				discos[i].setBounds(134,246,120,15);
				discos[i].setBackground(Color.green);
				break;
			}
			// este codigo es igual para cada disco, por tanto lo saco del case
			discos[i].setOpaque(true);
			contentPane.add(discos[i]);
			
		}
		
	// luego de terminar de crear y colocar los labels, como es el inicio del  juego, llenamos la pila de la torre 1
		torre1.push(discos[2]);
		torre1.push(discos[1]);
		torre1.push(discos[0]);
		// el metodo add agrega un dato al final de la lista, por lo que mi primer elemento debe ser el disco mas peque�o 
		
	}
	
	// vamos a crear por cada torre su verificacion si un disco se puede colocar encima o no
	// � como sabes si el tama�o es mayor o  no? Para eso usamos el largo del disco, con la propiedad width obtenes el largo y ya podemos comparar
	
	
	public void comparar_torre2(JLabel disco,int torreproveniente) {
		// preguntamos si esta vacia o no, si esta vacia simplemente lo insertamos y movemos el disco
		// el truco para colocar en que posicion va cada disco es sumar un valor siempre a X y a Y partiendo de la base de cada torre
		// para esta caso yo le di un espacio de 20 pixeles de suma a cada disco y a Y es siempre la altura de cada disco para que se vean pegados
		
		
		// para la torre 2, 319,261 son mi base
		if(torre2.size() == 0) {
				int size_disk = disco.getWidth();
				switch(size_disk){
					case 40:
							disco.setLocation(319+20*2,261-15*1); // 20*2 por que es mi disco mas peque�o y 20 pixeles es el tama�o de separo, 7 por la altura del label, y como no hay nada le doy *1
						break;
					case 80:
							disco.setLocation(319+20*1,261-15*1);
						break;
					case 120:
							disco.setLocation(319+20*0,261-15*1);
						break;
				}
				elimininar_proveniente(torreproveniente);
				
				torre2.push(disco);
		}else {
				
			// pregutamos si el tama�o del primer disco es menor al que esta
			int tama�o_disco_que_viene = disco.getWidth();
			int tama�o_disco_cabeza = torre2.peek().getWidth();
			if(tama�o_disco_que_viene > tama�o_disco_cabeza) {
				// lo mando a la torre 3 0 2
				if(torreproveniente == 3) {
					comparar_torre1(disco,torreproveniente);
				}else if(torreproveniente ==1) {
					comparar_torre3(disco,torreproveniente);
				}
				
			}else {
				// si lo agrego
				int numerodiscosentorre = torre2.size()+1;
				switch(tama�o_disco_que_viene){
				case 40:
						disco.setLocation(319+20*2,261-15*numerodiscosentorre); // 20*2 por que es mi disco mas peque�o y 20 pixeles es el tama�o de separo, 7 por la altura del label, y como no hay nada le doy *1
					break;
				case 80:
						disco.setLocation(319+20*1,261-15*numerodiscosentorre);
					break;
				case 120:
						disco.setLocation(319+20*0,261-15*numerodiscosentorre);
					break;
				}
				elimininar_proveniente(torreproveniente);
				torre2.push(disco);
			}			
		}
	}
	
	
	public void comparar_torre3(JLabel disco,int torreproveniente) {
		// para la torre 2, 531,261 son mi base
		if(torre3.size() == 0) {
				int size_disk = disco.getWidth();
				switch(size_disk){
					case 40:
							disco.setLocation(531+20*2,261-15*1); 
						break;
					case 80:
							disco.setLocation(531+20*1,261-15*1);
						break;
					case 120:
							disco.setLocation(531+20*0,261-15*1);
						break;
				}
				// por ultimo insertamos el disco en la torre 3
				elimininar_proveniente(torreproveniente);
				torre3.push(disco);
				
		}else {

			int tama�o_disco_que_viene = disco.getWidth();
			int tama�o_disco_cabeza = torre3.peek().getWidth();
			if(tama�o_disco_que_viene > tama�o_disco_cabeza) {
				// en este caso como no hay mas torres, solo no se hace el movimiento, no se mueve
			}else {
				
				int numerodiscosentorre = torre3.size()+1;
				switch(tama�o_disco_que_viene){
				case 40:
						disco.setLocation(531+20*2,261-15*numerodiscosentorre); // 20*2 por que es mi disco mas peque�o y 20 pixeles es el tama�o de separo, 7 por la altura del label, y como no hay nada le doy *1
					break;
				case 80:
						disco.setLocation(531+20*1,261-15*numerodiscosentorre);
					break;
				case 120:
						disco.setLocation(531+20*0,261-15*numerodiscosentorre);
					break;
				}
				elimininar_proveniente(torreproveniente);
				torre3.push(disco);
			}			
		}
	}
	
	
	
	public void comparar_torre1(JLabel disco,int torreproveniente) {
		// para la torre 2, 531,261 son mi base
		if(torre1.size() == 0) {
				int size_disk = disco.getWidth();
				switch(size_disk){
					case 40:
							disco.setLocation(134+20*2,261-15*1); 
						break;
					case 80:
							disco.setLocation(134+20*1,261-15*1);
						break;
					case 120:
							disco.setLocation(134+20*0,261-15*1);
						break;
				}
				// por ultimo insertamos el disco en la torre 3
				elimininar_proveniente(torreproveniente);
				torre1.push(disco);
				
		}else {

			int tama�o_disco_que_viene = disco.getWidth();
			int tama�o_disco_cabeza = torre1.peek().getWidth();
			if(tama�o_disco_que_viene > tama�o_disco_cabeza) {
				// no hago nada
			}else {
				
				int numerodiscosentorre = torre1.size()+1;
				switch(tama�o_disco_que_viene){
				case 40:
						disco.setLocation(134+20*2,261-15*numerodiscosentorre); // 20*2 por que es mi disco mas peque�o y 20 pixeles es el tama�o de separo, 7 por la altura del label, y como no hay nada le doy *1
					break;
				case 80:
						disco.setLocation(134+20*1,261-15*numerodiscosentorre);
					break;
				case 120:
						disco.setLocation(134+20*0,261-15*numerodiscosentorre);
					break;
				}
				elimininar_proveniente(torreproveniente);
				torre1.push(disco);
			}			
		}
	}
	
	
	public void elimininar_proveniente(int torre){
		switch(torre) {
		case 1:			
			torre1.pop();
			break;
		case 2:
			torre2.pop();
			break;
		case 3:
			torre3.pop();
			break;
		}
	}

	
	public void controltiempo() {
		
		labelhora.setText(String.valueOf(tiempojuego));
		while(terminatiempo ==false && tiempojuego >=0) {
			
			int timepores = Integer.parseInt(labelhora.getText());
			timepores--;
			tiempojuego = timepores;
			labelhora.setText(String.valueOf(timepores));
			
			try {
				hilohora.sleep(1000);
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void potenciadores() {
		// voy a crear potenciadores cada 5 segundos, hasta que el juego termine o pierda
		while(terminatiempo==false) {
			Potenciador_Debilitador nuevo = new Potenciador_Debilitador(contentPane,labelhora);
			nuevo.start();
		
			try {
				hilopotenciador.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		Thread ct = Thread.currentThread();
		
		if(ct == hilopotenciador) {
			potenciadores();
		}
		
		if(ct == hilohora) {
			controltiempo();
		}
		
	}
}
