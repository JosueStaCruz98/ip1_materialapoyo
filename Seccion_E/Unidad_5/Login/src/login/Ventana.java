/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

/**
 *
 * @author chechajosue
 */
public class Ventana {
    
    final String usuario = "admin";
    final String pwd = "admin";
    
    JFrame ventanaInicio;
    JFrame ventanPrincipal;
    JLabel fondoL;
    JPanel contenedor1;
    JPanel contenedor2;
    
    JButton botonLogin;
    JTextField txtUsuario;
    JPasswordField txtPassword;
    
    JTabbedPane miPanelPrincipal;
    
    public Ventana(){
        
        // Ventana de login
        ventanaInicio = new JFrame("Login");
        ventanaInicio.setBounds(0, 0, 1000, 600);
        ventanaInicio.setVisible(true);
        ventanaInicio.setLayout(null);
        ventanaInicio.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        // Ventana del programa
        ventanPrincipal = new JFrame("Menú Principal");
        ventanPrincipal.setBounds(0, 0, 1000, 600);
        ventanPrincipal.setVisible(false);
        ventanPrincipal.setLayout(null);
        ventanPrincipal.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        // Pestañas
        miPanelPrincipal = new JTabbedPane();
        miPanelPrincipal.setBounds(0, 0, 1000, 600);
        miPanelPrincipal.setVisible(true);
        
        ventanaInicio.add(miPanelPrincipal);
        
        contenedor1 = new JPanel();
        contenedor1.setBounds(0,0,1000,600);
        contenedor1.setVisible(true);
        contenedor1.setBackground(Color.BLUE);
        
        contenedor2 = new JPanel();
        contenedor2.setBounds(0,0,1000,600);
        contenedor2.setVisible(true);
        contenedor2.setBackground(Color.RED);
        
        // Pestaña 1
        miPanelPrincipal.addTab("Iniciar sesión", contenedor1);
        
        // Pestaña 2
        miPanelPrincipal.addTab("Ayuda", contenedor2);
        
        fondoL = new JLabel();
        fondoL.setBounds(0, 0, 1000, 600);
        fondoL.setVisible(true);
        fondoL.setLayout(null);
        fondoL.setIcon(redimensionarImagen(1000,600, "fondo.jpg"));
        
        contenedor1.add(fondoL);
        
        botonLogin = new JButton("Login");
        botonLogin.setBounds(450, 350, 100, 25);
        botonLogin.setVisible(true);
        botonLogin.setLayout(null);
        
        botonLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                // El texto de los texfield
                String usuario_txt = txtUsuario.getText();
                String pwd_txt = txtPassword.getText();
                
                // Login exitoso
                if(usuario_txt.equals(usuario) && pwd_txt.equals(pwd)){
                    JOptionPane.showMessageDialog(ventanaInicio, "Bienvenido Admin");
                    ventanPrincipal.setVisible(true);
                    ventanaInicio.setVisible(false);
                }else{
                    JOptionPane.showMessageDialog(ventanaInicio, "Credenciales incorrectas");
                }
            }
        });
        
        fondoL.add(botonLogin);
        
        txtUsuario = new JTextField();
        txtUsuario.setBounds(350, 150, 300, 50);
        txtUsuario.setVisible(true);
        txtUsuario.setLayout(null);
        
        txtPassword = new JPasswordField();
        txtPassword.setBounds(350, 250, 300, 50);
        txtPassword.setVisible(true);
        txtPassword.setLayout(null);
        
        fondoL.add(txtUsuario);
        fondoL.add(txtPassword);
        
        ventanaInicio.repaint();
    }
    
    public ImageIcon redimensionarImagen(int x, int y, String url){
        ImageIcon a = new ImageIcon(url);
        return new ImageIcon(a.getImage().getScaledInstance(x, y, Image.SCALE_DEFAULT));
    }
}
