/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablasgraficas;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import javax.swing.JOptionPane;
/**
 *
 * @author chechajosue
 */
public class Lector {

    public Lector() {
    }

    public static Carro[] parsearJSON(String textoJSON) {

        Gson gson = new Gson();
        Carro carros[] = gson.fromJson(textoJSON, Carro[].class);

        for (Carro carro : carros) {
            System.out.println(carro);
        }

        return carros;
    }

    public static String leerArchivo(String ruta) throws IOException {
        String texto = "";
        BufferedReader lector = null;

        try {
            File archivo = new File(ruta);
            lector = new BufferedReader(new FileReader(archivo));

            String linea = lector.readLine();

            while (linea != null) {
                texto += linea;
                linea = lector.readLine();
            }
        } catch (Exception e) {
            System.out.println("Ocurrio un error.");
            e.printStackTrace();
        } finally {
            if (lector != null) {
                lector.close();
            }
        }

        return texto;
    }
    
    public static void generarPDF(Carro [] carros, String ruta){
        
        Document documento = new Document();
        
        try {
            
            PdfWriter.getInstance(documento, new FileOutputStream(ruta));
            documento.open();
            
            PdfPTable tablaCarros = new PdfPTable(4);
            tablaCarros.addCell("ID");
            tablaCarros.addCell("Fabricante");
            tablaCarros.addCell("Puertas");
            tablaCarros.addCell("Precio");
            
            for (int i = 0; i < carros.length; i++) {
                if(carros[i] != null){
                    tablaCarros.addCell(String.valueOf(carros[i].getId()));
                    tablaCarros.addCell(String.valueOf(carros[i].getFabricante()));
                    tablaCarros.addCell(String.valueOf(carros[i].getPuertas()));
                    tablaCarros.addCell(String.valueOf(carros[i].getPrecio()));
                }
            }
            
            documento.add(tablaCarros);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        documento.close();
        JOptionPane.showMessageDialog(null, "Reporte pdf creado");
        
    }
}
