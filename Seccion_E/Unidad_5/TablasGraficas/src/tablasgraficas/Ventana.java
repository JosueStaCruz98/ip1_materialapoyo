/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablasgraficas;

import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Locale;
import javax.swing.*;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 *
 * @author chechajosue
 */
public class Ventana {
    
    Lector lector;
    
    JLabel textoTabla;
    JLabel textoMenu;
    
    JFrame ventanaInicio;
    JPanel contenedorTabla;
    JPanel contenedorMenu;
    JButton botonCargar;
    JButton botonGraficar;
    JButton botonPDF;
    JButton botonLimpiar;
    
    DefaultTableModel modeloTabla;
    JTable tabla;
    
    Carro carros [] = new Carro[100];
    
    public Ventana(){
        
        lector = new Lector();
        ventanaInicio = new JFrame("Datos de carros");
        ventanaInicio.setBounds(0, 0, 1000, 600);
        ventanaInicio.setLayout(null);
        ventanaInicio.setDefaultCloseOperation(EXIT_ON_CLOSE);
        ventanaInicio.setLocationRelativeTo(null);
        ventanaInicio.setVisible(true);
        
        contenedorTabla = new JPanel();
        contenedorTabla.setBounds(50, 50, 500, 500);
        contenedorTabla.setLayout(new BorderLayout());
        contenedorTabla.setBackground(Color.LIGHT_GRAY);
        contenedorTabla.setVisible(true);
        
        contenedorMenu = new JPanel();
        contenedorMenu.setBounds(600, 0, 400, 600);
        contenedorMenu.setLayout(null);
        contenedorMenu.setBackground(Color.LIGHT_GRAY);
        contenedorMenu.setVisible(true);
        
        textoTabla = new JLabel("Tabla de Carros");
        textoTabla.setBounds(250, 20, 150, 25);
        textoTabla.setLayout(null);
        textoTabla.setVisible(true);
        
        textoMenu = new JLabel("Menú");
        textoMenu.setBounds(175, 20, 100, 25);
        textoMenu.setLayout(null);
        textoMenu.setVisible(true);
        
        ventanaInicio.add(textoTabla);
        contenedorMenu.add(textoMenu);
        
        botonCargar = new JButton("Cargar datos");
        botonCargar.setBounds(125, 100, 150, 25);
        botonCargar.setLayout(null);
        botonCargar.setVisible(true);
        
        botonGraficar = new JButton("Graficar");
        botonGraficar.setBounds(125, 175, 150, 25);
        botonGraficar.setLayout(null);
        botonGraficar.setVisible(true);
        
        botonPDF = new JButton("Generar PDF");
        botonPDF.setBounds(125, 250, 150, 25);
        botonPDF.setLayout(null);
        botonPDF.setVisible(true);
        
        botonLimpiar = new JButton("Limpiar datos");
        botonLimpiar.setBounds(125, 325, 150, 25);
        botonLimpiar.setLayout(null);
        botonLimpiar.setVisible(true);
        
        botonCargar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cargarDatos();
                JOptionPane.showMessageDialog(ventanaInicio, "Datos cargados correctamente.");
            }
        });
        
        botonGraficar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Graficando...");
//                GraficaPie grafica = new GraficaPie("Puertas carros", carros);
//                grafica.setSize(500, 300);
//                grafica.setVisible(true);
                GraficaBarras grafica = new GraficaBarras("Puertas carros", carros);
                grafica.setSize(500, 300);
                grafica.setVisible(true);
            }
        });
        
        botonPDF.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Generando PDF...");
                Lector.generarPDF(carros, "reporteCarros.pdf");
            }
        });
        
        botonLimpiar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                System.out.println("Limpiando tabla...");
                limpiarFilas();
            }
        });
        
        contenedorMenu.add(botonCargar);
        contenedorMenu.add(botonGraficar);
        contenedorMenu.add(botonPDF);
        contenedorMenu.add(botonLimpiar);
        
        modeloTabla = new DefaultTableModel();
        tabla = new JTable(modeloTabla);
        
        modeloTabla.addColumn("ID");
        modeloTabla.addColumn("Fabricante");
        modeloTabla.addColumn("Puertas");
        modeloTabla.addColumn("Precio (Q)");
        
        contenedorTabla.add(new JScrollPane(tabla));
        contenedorTabla.repaint();
        
        ventanaInicio.add(contenedorTabla);
        ventanaInicio.add(contenedorMenu);
        contenedorTabla.setVisible(true);
        contenedorMenu.setVisible(true);
        ventanaInicio.setVisible(true);
        ventanaInicio.repaint();
    }
    
    void cargarDatos(){
        limpiarFilas();
        String texto = "";
        
        try {
            // Texto plano
            texto = lector.leerArchivo("Carros.json");
            
            // Convertir a arreglo de carros, desde un JSON
            carros = lector.parsearJSON(texto);
            
            int indice = 0;
            
            for (Carro carro : carros) {
                if(carro!=null){
                    modeloTabla.insertRow(indice, carro.toArreglo());
                    indice++;
                }
            }
            
        } catch (Exception e) {
        
            System.out.println("Ocurrió un error");
            e.printStackTrace();
        }
    }
    
    void limpiarFilas(){
        for (int i = modeloTabla.getRowCount()-1; i >= 0; i--) {
            modeloTabla.removeRow(i);
        }
    }
}

class GraficaPie extends JFrame {
    
    Carro carros [];
    
    public GraficaPie(String tituloVentana, Carro carros []){
        super(tituloVentana);
        this.carros = carros;
        setContentPane(crearPanel());
        this.repaint();
    }
    
    JPanel crearPanel(){
        JFreeChart grafica = crearChart(crearDataSet());
        return new ChartPanel(grafica);
    }
    
    JFreeChart crearChart(PieDataset dataset){
        JFreeChart chart = ChartFactory.createPieChart("Cantidad de puertas por carro", dataset, true, true, true);
        return chart;
    }
    
    PieDataset crearDataSet(){
        
        DefaultPieDataset dataset = new DefaultPieDataset();
        int contador_1puerta = 0, contador_2puertas = 0, contador_3puertas = 0, contador_4puertas = 0;
        
        for (Carro carro : carros) {
            switch(carro.getPuertas()){
                // Si el carro tiene 1 puerta
                case 1:
                    contador_1puerta++;
                    break;
                    
                // Si el carro tiene 2 puertas
                case 2:
                    contador_2puertas++;
                    break;
                    
                // Si el carro tiene 3 puertas
                case 3:
                    contador_3puertas++;
                    break;
                
                // Si el carro tiene 4 puertas
                case 4:
                    contador_4puertas++;
                    break;
            }
        }
        dataset.setValue("Una puerta", contador_1puerta);
        dataset.setValue("Dos puertas", contador_2puertas);
        dataset.setValue("Tres puertas", contador_3puertas);
        dataset.setValue("Cuatro puertas", contador_4puertas);
        return dataset;
    }
}


class GraficaBarras extends JFrame {
    
    Carro carros [];
    
    public GraficaBarras(String tituloVentana, Carro carros []){
        super(tituloVentana);
        this.carros = carros;
        setContentPane(crearPanel());
        this.repaint();
    }
    
    JPanel crearPanel(){
        JFreeChart grafica = crearChart(crearDataSet());
        return new ChartPanel(grafica);
    }
    
    JFreeChart crearChart(CategoryDataset dataset){
        JFreeChart barChart = ChartFactory.createBarChart("Cantidad de puertas por carro", "", "Puertas", dataset, PlotOrientation.VERTICAL, false, true, false);
        return barChart;
    }
    
    CategoryDataset crearDataSet(){
        
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        int contador_1puerta = 0, contador_2puertas = 0, contador_3puertas = 0, contador_4puertas = 0;
        
        for (Carro carro : carros) {
            switch(carro.getPuertas()){
                // Si el carro tiene 1 puerta
                case 1:
                    contador_1puerta++;
                    break;
                    
                // Si el carro tiene 2 puertas
                case 2:
                    contador_2puertas++;
                    break;
                    
                // Si el carro tiene 3 puertas
                case 3:
                    contador_3puertas++;
                    break;
                
                // Si el carro tiene 4 puertas
                case 4:
                    contador_4puertas++;
                    break;
            }
        }
        
        dataset.setValue(contador_1puerta, "Puertas", "1 puerta");
        dataset.setValue(contador_2puertas, "Puertas", "2 puertas");
        dataset.setValue(contador_3puertas, "Puertas", "3 puertas");
        dataset.setValue(contador_4puertas, "Puertas", "4 puertas");
        
        return dataset;
    }
}