/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesobjetos;

/**
 *
 * @author chechajosue
 */
public class ClasesObjetos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Carro carro = new Carro("Yaris", "Gris", "Toyota", "001", 70000, 150000, 0);
        Carro carro1 = new Carro();
        
        // Obtenes la informacion
        carro1.setFabricante("Mazda");
        carro1.setColor("Rosado");
        
        System.out.println(carro.getModelo());
        System.out.println(carro.getFabricante());
        System.out.println(carro.getColor());
        System.out.println(carro.getKilometraje(false));
        System.out.println(carro.getKilometraje(true) + "\n");
        
        System.out.println(carro.toString());
    }   
}

class Carro {
    
    // Atributos
    private String modelo, color, fabricante, placa;
    private double precio, kilometraje, gasolina;
    
    // Metodos y funciones
    
    //Constructor 
    public Carro (){
        
    }
    
    public Carro(String modelo, String color, String fabricante, String placa, double precio, double kilometraje, double gasolina) {
        this.modelo = modelo;
        this.color = color;
        this.fabricante = fabricante;
        this.placa = placa;
        this.precio = precio;
        this.kilometraje = kilometraje;
        this.gasolina = gasolina;
    }

    @Override
    public String toString(){
        return this.fabricante + "\n" + this.modelo + "\n" + this.color;
    }
    
    public String getModelo() {
        return modelo;
    }

    public String getColor() {
        return color;
    }

    public String getFabricante() {
        return fabricante;
    }

    public String getPlaca() {
        return placa;
    }

    public double getPrecio() {
        return precio;
    }

    public double getKilometraje(boolean millas) {
        return millas ? kilometraje*1.6 : kilometraje;
    }

    public double getGasolina() {
        return gasolina;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setKilometraje(double kilometraje) {
        this.kilometraje = kilometraje;
    }
    
    
    // carro.gasolina = 3;
    // carro.setGasolina(3);
    public void setGasolina(double gasolina) {
        this.gasolina = gasolina;
    }
}