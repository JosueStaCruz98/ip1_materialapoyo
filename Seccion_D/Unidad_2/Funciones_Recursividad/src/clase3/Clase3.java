/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3;

/**
 *
 * @author fernando
 */
public class Clase3 {

    int global;

    public static void main(String[] args) {
        int[] lista = {1, 3, 5, 7, 9};
        //System.out.println(sumaRecursiva(lista));
        imprimirNumeros(5);
        factorial(3);
        sumaIterativa(lista);
        sumaRecursiva(lista);
    }

    public static int suma3Numeros(int num1, int num2, int num3) {
        int resultado = num1 + num2 + num3;
        return resultado;
    }
    //Realizar una suma con ciclos de forma iterativa
    public static int sumaIterativa(int[] listaNumeros) {
        int suma = 0;
        for (int i = 0; i < listaNumeros.length; i++) {   //lista.length = 5    

            //System.out.print("suma = " + suma + " + " + listaNumeros[i]);
            suma = suma + listaNumeros[i];
            //System.out.println(" = " + suma);
        }
        return suma;
    }
    /*
	        para acceder a una lista tenemos estas posiciones
	        
	        0   ->     length - 1
    
	suma = 0
	suma = 0 + 1 = 1
	suma = 1 + 3 = 4 
	suma = 4 + 5 = 9
	suma = 9 + 7 = 16
	suma = 16 + 9 = 25
	
    */
    //Realizar una suma de forma recursiva
    public static int sumaRecursiva(int[] listaNumeros) {
        if (listaNumeros.length == 1) {
            return listaNumeros[0];
        } else {
            int[] restoLista = new int[listaNumeros.length - 1];
            for (int i = 1; i < listaNumeros.length; i++) {
                restoLista[i - 1] = listaNumeros[i];
            }

            return listaNumeros[0] + sumaRecursiva(restoLista);
        }

    }
    //Declaracion de un metodo
    public static void otroMetodo(int a) {
        System.out.println("Soy un metodo y recibi a: " + a);
        return;
    }

    //Declaracion de una funcion
    public static int otraFuncion(int a) {
        System.out.println("Soy una funcion y recibi a: " + a);
        return a + 1;
    }

    //Encontrar el factorial de un numero: formula:  n * (n-1)!
    public static int factorial(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }

    public static void imprimirNumeros(int numero) {
        if (numero >= 0) {
            System.out.println(numero);
            imprimirNumeros(numero - 1);
        }
    }

    
    public static void estructurasControl() {
        System.out.println("Hello World");

        int a = 2;

        if (a == 0) {
            int b = 1;
            if (b == 2) {
                //int b = 3;
                System.out.println(a + b);
            } else {
                int c = 4;
                System.out.println(a + c);
            }
            System.out.println(b);
        } else {
            int b = 5;
            System.out.println(a + b);
        }
        System.out.println(suma3Numeros(a, a, 5));
        otroMetodo(a);
    }

}
