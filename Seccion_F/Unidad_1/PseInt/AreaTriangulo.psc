Algoritmo AreaTriangulo
	Escribir "----------- Calcular el area de un triangulo -----------"
	Definir base, altura, resultado Como Real
	
	Escribir "Escriba la base del triangulo en cm: "
	Leer base
	Escribir "Escriba la altura del triangulo en cm: "
	Leer altura
	
	resultado <- (base * altura) / 2
	
	Escribir "El area del triangulo es: ", resultado, " cm"
FinAlgoritmo
