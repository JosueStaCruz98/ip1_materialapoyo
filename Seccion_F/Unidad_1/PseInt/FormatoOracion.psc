// Arreglar el formato de una oracion
// 		Una oracion con un formato correcto empieza
// 		con letra inicial mayuscula y lo demas en minuscula.
// Faltan cosas por considerar pero se hara de manera simple.

// Ejemplo:
// El perro es grande.
// el peRRo es grandE.
Algoritmo FormatoOracion
	Definir oracionIncorrecta, primerLetra, resto, oracionCorrecta Como Caracter
	
	Escribir "Escribe una oracion"
	Leer oracionIncorrecta
	
	// Obteniendo la primera letra de un texto
	primerLetra <- SubCadena(oracionIncorrecta, 1, 1)
	resto <- SubCadena(oracionIncorrecta, 2, Longitud(oracionIncorrecta))
	
	primerLetra <- Mayusculas(primerLetra)
	resto <- Minusculas(resto)
	
	oracionCorrecta <- Concatenar(primerLetra, resto)
	Escribir oracionCorrecta
FinAlgoritmo
