package com.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Arreglos {

    public static void main(String[] args) {
        // Manejo de errores con try-catch-finally
        // Ejemplo: Solicitar dos numeros al usuario hasta que sean correctos
        int num1 = 0, num2 = 0;
        boolean datosCorrectos = false;
        while (!datosCorrectos) {
            try {
                Scanner scanner = new Scanner(System.in);
                num1 = scanner.nextInt();
                num2 = scanner.nextInt();
                datosCorrectos = true;
            } catch (InputMismatchException e) {
                System.out.println("Ingrese un numero valido: " + e.getMessage());
            } catch (Exception e) {
                System.out.println("Error en el programa" + e.getMessage());
            } finally {
                System.out.println("Esto se ejecuta siempre");
            }
        }
    }
}
